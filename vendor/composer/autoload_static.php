<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit67ee4dc133a854a91586f5c2fbe73b2f
{
    public static $prefixLengthsPsr4 = array (
        'B' => 
        array (
            'Beweb\\Td\\Models\\Race\\Impl\\' => 26,
            'Beweb\\Td\\Models\\Job\\Impl\\' => 25,
            'Beweb\\Td\\Models\\Interface\\' => 26,
            'Beweb\\Td\\Models\\' => 16,
            'Beweb\\Td\\Duels\\' => 15,
            'Beweb\\Td\\Dal\\' => 13,
            'Beweb\\Td\\' => 9,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Beweb\\Td\\Models\\Race\\Impl\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src/models/impl',
        ),
        'Beweb\\Td\\Models\\Job\\Impl\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src/models/impl',
        ),
        'Beweb\\Td\\Models\\Interface\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src/models/interfaces',
        ),
        'Beweb\\Td\\Models\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src/models',
        ),
        'Beweb\\Td\\Duels\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src/Duels',
        ),
        'Beweb\\Td\\Dal\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src/dal',
        ),
        'Beweb\\Td\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static $classMap = array (
        'Beweb\\Td\\Dal\\Dao' => __DIR__ . '/../..' . '/src/dal/Dao.php',
        'Beweb\\Td\\Dal\\DaoCharacter' => __DIR__ . '/../..' . '/src/dal/DaoCharacter.php',
        'Beweb\\Td\\Dal\\DaoJob' => __DIR__ . '/../..' . '/src/dal/DaoJob.php',
        'Beweb\\Td\\Dal\\DaoRace' => __DIR__ . '/../..' . '/src/dal/DaoRace.php',
        'Beweb\\Td\\Models\\Character' => __DIR__ . '/../..' . '/src/models/Character.php',
        'Beweb\\Td\\Models\\Interface\\Fighter' => __DIR__ . '/../..' . '/src/models/interfaces/Fighter.php',
        'Beweb\\Td\\Models\\Interface\\ModifierInterface' => __DIR__ . '/../..' . '/src/models/interfaces/ModifierInterface.php',
        'Beweb\\Td\\Models\\Interface\\Updatable' => __DIR__ . '/../..' . '/src/models/interfaces/Updatable.php',
        'Beweb\\Td\\Models\\Job' => __DIR__ . '/../..' . '/src/models/Job.php',
        'Beweb\\Td\\Models\\Job\\Impl\\Archer' => __DIR__ . '/../..' . '/src/models/impl/Archer.php',
        'Beweb\\Td\\Models\\Job\\Impl\\Warlock' => __DIR__ . '/../..' . '/src/models/impl/Warlock.php',
        'Beweb\\Td\\Models\\Job\\Impl\\Warrior' => __DIR__ . '/../..' . '/src/models/impl/Warrior.php',
        'Beweb\\Td\\Models\\Race' => __DIR__ . '/../..' . '/src/models/Race.php',
        'Beweb\\Td\\Models\\Race\\Impl\\Alf' => __DIR__ . '/../..' . '/src/models/impl/Alf.php',
        'Beweb\\Td\\Models\\Race\\Impl\\Dwarf' => __DIR__ . '/../..' . '/src/models/impl/Dwarf.php',
        'Beweb\\Td\\Models\\Race\\Impl\\Human' => __DIR__ . '/../..' . '/src/models/impl/Human.php',
        'Beweb\\Td\\Models\\Stats' => __DIR__ . '/../..' . '/src/models/Stats.php',
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit67ee4dc133a854a91586f5c2fbe73b2f::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit67ee4dc133a854a91586f5c2fbe73b2f::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInit67ee4dc133a854a91586f5c2fbe73b2f::$classMap;

        }, null, ClassLoader::class);
    }
}
