<?php 

namespace Beweb\Td\Models\Interface;

interface Fighter {
    function attack(Fighter &$target): void;
}