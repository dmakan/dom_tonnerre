<?php
namespace Beweb\Td\Duels;
namespace Beweb\Td\Dal;
use Beweb\Td\Dal\DaoCharacter;

class Duel extends DaoCharacter
{
    public Character $fighter;
    public Character $opposent;

    function __construct()
    {
        $this->fighter = $this->createCharacter('elf', 'archer');
        $this->oppossent = $this->createCharacter('dwarf', 'warrior');
        $this->start;
    }
}