<?php

    namespace Beweb\Td\Dal;
    /**
     * class Data sert à ecrire, lire, update dans un fichier
     * param $dataSource : chemin de fichier
     */

    abstract class Dao 
    {
        private string $dataSource;
        abstract public function persist(mixed $data);
        abstract public function load();
    }
