<?php 
    namespace Beweb\Td\Dal;
    use Beweb\Td\Models\Race;

    class DaoRace extends Dao {


        function __construct(){
            // source de données
            $this->datasource = "./db/races.json";
        }

        // persister les données
        function persist(mixed $data){

        }
        
        // recuperation des données
        function load(): array{
            $races = [];
            $datas = json_decode(file_get_contents($this->datasource),true);
            foreach ($datas as  $race_as_array) {
                $r = new Race();
                $r->name = $race_as_array["name"];
                $r->id = $race_as_array["id"];

                array_push($races, $r);
            }

            return $races;
        }

        // research by id
        function find_by_id($id) {
            $data = file_get_contents($this->datasource);
            $data_json = json_decode($data, true);

            foreach ($data_json as $data)
            {
                if ($data->id == $id)
                {
                    $job = new Job();
                    $job->name = $data["name"];
                    $job->attack = $data["att"];
                    $job->pv = $data["pv"];
                    $job->def = $data["def"];

                    return $job;
                }
                
            }

            return null;
        }

        // research by name
        function find_by_name($name) {
            
            foreach ($this->load() as $data)
            {
                if ($data->name == strtolower($name))
                {
                    return $data;
                } 

            }

            return "Not find !";
        }
    }