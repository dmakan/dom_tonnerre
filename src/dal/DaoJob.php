<?php 
    namespace Beweb\Td\Dal;
    use Beweb\Td\Models\Job;

    class DaoJob extends Dao {


        function __construct(){
            // source de données
            $this->datasource = "./db/jobs.json";
        }

        // persister les données
        function persist(mixed $data){

        }
        
        // recuperation des données
        function load(): array{
            $jobs = [];
            $datas = json_decode(file_get_contents($this->datasource),true);
            foreach ($datas as  $job_as_array) {
                $j = new Job();
                $j->att = $job_as_array["att"];
                $j->def = $job_as_array["def"];
                $j->pv = $job_as_array["pv"];
                $j->name = $job_as_array["name"];
                array_push($jobs,$j);
            }
            return $jobs;
        }

        // research by id
        function find_by_id($id) {
            foreach ($this->load() as $data)
            {
                if ($data->id == $id)
                {
                    $job = new Job();
                    $job->name = $data["name"];
                    $job->attack = $data["att"];
                    $job->pv = $data["pv"];
                    $job->def = $data["def"];

                    return $job;
                }
                
            }

            return null;
        }

        // research by name
        function find_by_name($name) {
            foreach ($this->load() as $data)
            {
                if ($data->name == strtolower($name))
                {
                    return $data;
                } 

            }

            return "Not find !";
        }
    }