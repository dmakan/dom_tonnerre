<?php 
    namespace Beweb\Td\Dal;
    // namespace Beweb\Td\Models;

    use Beweb\Td\Dal\DaoRace;
    use Beweb\Td\Dal\DaoJob;
    use Beweb\Td\Models\Character;
    use Beweb\Td\Dal\Dao;

     class DaoCharacter extends Dao
     {
        
        public $datasource = "../db/character.json";

        // chargement du character
        function load(): array{
            $characters = [];
            $datas = json_decode(file_get_contents($this->datasource),true);
            foreach ($datas as  $character_as_array) {

                $c = new Character(new Race(), new Job());
                $c->id = $character_as_array["id"];
                $c->race = $character_as_array["race"];
                $c->job = $character_as_array["job"];
                // $c->stat = $character_as_array["stats"];
                array_push($characters,$c);
            }
            return $characters;
        }

        // recherche par l'id
        function find_by_id($id): mixed
        {
            foreach ($this->load() as $character)
            {
                if ($data['id'] == $id)
                {

                    return $character;
                }
            }

            return null;
        }


        // persiste les data
        function persist(mixed $data){
            $characters = $this->load();
            $data->id = count($characters) +1;
            array_push($characters,$data);
            file_put_contents($this->datasource,json_encode($characters));
        }

        /**
         * Ici on implémente une methode pour creer un personnage 
         * oui le DAO est un gestionnaire d'entité , il semble normal de lui deléguer 
         * la fastidieuse tache de fabriquer des personnages (oui ce serait con de faire ça dans l'index.php alors qu'on sait faire des objets ;) )
         * 
         * @todo faire en sorte de passer par un objet qui retourne le bon DAO selon ce qu'on veut faire
         * au lieu de les instanciers dans les methodes (cf : fabrique , singleton ).
         *
         * @param string $race idem que pour job mais pour  race , sauf que c'est différent parce que c'est pas pareil alors que les structures sont les mêmes, mais c'est tout a faire similaire malgré le corollaire évident de la propriété recherchée 
         * @param string $job la classe du personnage pour la chercher afin de l'ajouter a la création du personnage
         * @return Character le personnage que l'on vient de creer
         */
        function createCharacter(string $race , string $job): Character{
            $daoRace =  new DaoRace(); 
            $daoJob =  new DaoJob(); 
            $c = new Character(
                $daoRace->find_by_name($race),
                $daoJob->find_by_name($job));
            return $c;
        }

     }