<?php 



interface Persistable {
    function persist(Entity $entity): void;
}